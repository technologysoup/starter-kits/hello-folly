#include <folly/io/async/EventBase.h>
#include <iostream>

int main() {
  folly::EventBase evb;
  evb.runInEventBaseThread([] { std::cout << "Hello there" << std::endl; });
  std::thread evb_thread = std::thread([&]() { evb.loopOnce(); });

  evb_thread.join();

  return 0;
}
