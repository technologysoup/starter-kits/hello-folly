#include <folly/io/async/EventBase.h>
#include <iostream>
#include <memory>

class X : public std::enable_shared_from_this<X> {
public:
  X(folly::EventBase &evb, int val) : evb_(evb) {
    value_ = val;
    std::cout << "X ctor" << std::endl;
  }

  void init() {
    evb_.runInEventBaseThread([me = shared_from_this()]() {
      std::cout << "Value is: " << me->value_ << std::endl;
    });
  }

  ~X() {
    value_ = 0;
    std::cout << "X dtor" << std::endl;
  }

private:
  int value_;
  folly::EventBase &evb_;
};

int main() {
  folly::EventBase evb;
  {
    std::shared_ptr<X> x = std::make_shared<X>(evb, 3);
    x->init();
  }
  std::thread evb_thread = std::thread([&]() { evb.loopOnce(); });

  evb_thread.join();

  return 0;
}
