# How to use folly EventBase

Obtain and build folly.

Folly: https://github.com/facebook/folly

Set FOLLY\_INSTALL\_DIR. Run following comman from folly directory.

```
export FOLLY_INSTALL_DIR=$(python3 ./build/fbcode_builder/getdeps.py show-inst-dir)
```

Make sure to install required dependencies. These will be required when linking with folly.

Build:

```
mkdir build
cd build
cmake -DFOLLY_DIR=$FOLLY_INSTALL_DIR  ..
make
```
Build using clang + mold linker + link with clang address sanitizer:

```
mkdir build
cd build
cmake -DFOLLY_DIR=$FOLLY_INSTALL_DIR -DCMAKE_CXX_COMPILER=/usr/local/bin/clang++ -DCMAKE_C_COMPILER=/usr/local/bin/clang -DCMAKE_EXE_LINKER_FLAGS="-fuse-ld=mold  -fsanitize=address" -DCMAKE_CXX_FLAGS=-fsanitize=address ..
make
```

Run:

```
./evb_bad
./evb_good
./evb_best
```
