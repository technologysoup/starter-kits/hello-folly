#include <folly/io/async/EventBase.h>
#include <iostream>
#include <memory>

class X : public std::enable_shared_from_this<X> {
public:
  [[nodiscard]] static std::shared_ptr<X> create(folly::EventBase &evb,
                                                 int val) {
    auto obj = std::shared_ptr<X>(new X(evb, val));
    obj->init();
    return obj;
  }

  ~X() {
    value_ = 0;
    std::cout << "X dtor" << std::endl;
  }

private:
  int value_;
  folly::EventBase &evb_;

  X(folly::EventBase &evb, int val) : evb_(evb) {
    value_ = val;
    std::cout << "X ctor" << std::endl;
  }

  void init() {
    evb_.runInEventBaseThread([me = shared_from_this()]() {
      std::cout << "Value is: " << me->value_ << std::endl;
    });
  }
};

int main() {
  folly::EventBase evb;
  { std::shared_ptr<X> x = X::create(evb, 3); }
  std::thread evb_thread = std::thread([&]() { evb.loopOnce(); });

  evb_thread.join();

  return 0;
}
