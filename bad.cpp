#include <folly/io/async/EventBase.h>
#include <iostream>

class X {
public:
  X(folly::EventBase &evb, int val) : evb_(evb) {
    value_ = val;
    std::cout << "X ctor" << std::endl;
    evb.runInEventBaseThread(
        [&]() { std::cout << "Value is: " << value_ << std::endl; });
  }

  ~X() {
    value_ = 0;
    std::cout << "X dtor" << std::endl;
  }

private:
  int value_;
  folly::EventBase &evb_;
};

int main() {
  folly::EventBase evb;
  { X x(evb, 3); }
  std::thread evb_thread = std::thread([&]() { evb.loopOnce(); });

  evb_thread.join();

  return 0;
}
